function Vo = boost_stage(Vi, Ts)
  C2 = 47e-9;
  Rc2 = Ts/(2*C2);
  C3 = 4.7e-6;
  Rc3 = Ts/(2*C3);
  
  R4 = 56e3;
  Rp = 0;
  R6 = 2.7e3;

  G = 1/(Rc3+Rp+R6);
  Gi = Rc3*G;
  G3 = (Rp+R6)*Gi;
  Gx = R4*Rc2/(R4+Rc2);
  Ga = Gx/Rc3;
  
  N = length(Vi);
  Vo = zeros(N,1);
  xc2 = 0;
  xc3 = 0;

  for n=1:N
    Va = Gi*Vi(n,1) + G3*xc3;
    Vo(n,1) = Vi(n,1) + Ga*Va - Gx*(xc3-xc2);
    if Vo(n,1) > 4.5
      Vo(n,1) = 4.5;
    elseif Vo(n,1) < -4.5
      Vo(n,1) = -4.5;
    endif
    
    Vrc2 = Vo(n,1)-Vi(n,1);
    xc2 = Vrc2*2/Rc2 - xc2;
    xc3 = Va*2/Rc3 - xc3;
  endfor
  
  
endfunction
