function Vo = input_stage(Vi, Ts)
  R1 = 22e6;
  C1 = 0.1e-6;
  Rc1 = Ts / (2*C1);
  R2 = 10e6;
  R3 = 1e3;  
  
  xc1 = 0;
  
  N = length(Vi);
  Vo = zeros(N,1);
  
  for n=1:N
    Va = (1 + Rc1/R1)*Vi(n,1) - Rc1*xc1;
    Vo(n,1) = (1-R3/R2)*Va - Vi(n,1)*R3/R1;
    xc1 = (Vi(n,1) - Va)*2/Rc1 - xc1;
  endfor
  
  
endfunction
