function Vo = output_stage(Vi, Ts)
  C5 = 15e-6;
  Rc5 = Ts/(2*C5);
  R9 = 470;
  R10 = 10e3;

  Gi = (R9+R10)/(Rc5+R9+R10);
  Gc5 = Rc5*Gi;
  Go = R10/(R9+R10);
  
  N = length(Vi);
  Vo = zeros(N,1);
  xc5 = 0;
 
  for n=1:N
    Va = Gi*Vi(n,1) - Gc5*xc5;
    Vo(n,1) = Go*Va;
    
    xc5 = (Vi(n,1)-Va)*2/Rc5 - xc5;
  endfor
endfunction
