clc;clear; close all;
Fs = 44100;
Ts = 1/Fs;
f = 1000;
t = [0:Ts:0.05].';

Vi = 0.1* sin(2*pi*f*t);

inVo = input_stage(Vi, Ts);
boostVo = boost_stage(inVo, Ts);
outVo = output_stage(boostVo, Ts);

% Waveform
subplot(4,3,1);
plot(t,Vi);
title('Effect Vi');
subplot(4,3,2);
[Hi,Fi] = freqz(Vi,1,4096,Fs);
plot(Fi , 20*log10(abs(Hi)));
title('Effect Vi freq response');

subplot(4,3,3);
semilogx(Fi , 20*log10(abs(Hi)));
title('Effect Vi freq response');

% %%%%%%%%%%%%%
subplot(4,3,4);
plot(t,inVo);
title('Effect inVo');
subplot(4,3,5);
[Hi,Fi] = freqz(inVo,1,4096,Fs);
plot(Fi , 20*log10(abs(Hi)));
title('Effect inVo freq response');

subplot(4,3,6);
semilogx(Fi , 20*log10(abs(Hi)));
title('Effect inVo freq response');

% %%%%%%%%%%%%%
subplot(4,3,7);
plot(t,boostVo);
title('Effect boostVo');
subplot(4,3,8);
[Hi,Fi] = freqz(boostVo,1,4096,Fs);
plot(Fi , 20*log10(abs(Hi)));
title('Effect boostVo freq response');

subplot(4,3,9);
semilogx(Fi , 20*log10(abs(Hi)));
title('Effect boostVo freq response');

% %%%%%%%%%%%%%
subplot(4,3,10);
plot(t,outVo);
title('Effect outVo');
subplot(4,3,11);
[Hi,Fi] = freqz(outVo,1,4096,Fs);
plot(Fi , 20*log10(abs(Hi)));
title('Effect outVo freq response');

subplot(4,3,12);
semilogx(Fi , 20*log10(abs(Hi)));
title('Effect outVo freq response');